from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    return render(request,"index.html")

def gallery(request):
    return render(request, "gallery.html")

def favorites(request):
    return render(request, "favorites.html")